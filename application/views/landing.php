<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIAK | Main Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/square/blue.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/select2/dist/css/select2.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?php echo base_url(); ?>" class="navbar-brand"><b>Sistem Informasi Administrasi Kependudukan</b></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
    <!--  <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
            <li><a href="#">Link</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
                <li class="divider"></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
          <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
              <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
            </div>
          </form>
        </div> -->
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">

            <?php if(!$this->session->userdata('is_logged_in')) { ?>
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <span class="">Login</span>
              </a>
              <ul class="dropdown-menu">
                <form action="#" method="post">
                <!-- The user image in the menu -->
                <li class="user-header">
                    <p class="login-box-msg">Sign in to start your session <br>
                      </p>

                    <div class="box-body">
                      <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="username" placeholder="Username">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                      </div>
                      <div class="form-group has-feedback">
                        <input type="password" class="form-control" name="password" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                      </div>
                    </div>
                    <input type="hidden" id="custId" name="id" value="1">
                </li>
                <li class="user-footer">
                  <div class="col-md-1"> </div>
                  <div class=col-md-10>
                  <div class="pull-left">
                    <button type="submit" class="btn btn-default btn-flat">Login</button>
                  </div>
                  <div class="pull-right">
                    <button type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-info"> Daftar </button>
                  </div>
                </div>
                </li>
              </ul>
              </form>
            </li>
          <?php }
           else { ?> <li><a href="<?php echo base_url(); ?>login/logout">Logout</a></li>  <?php  } ?>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->


      <!-- Main content -->
      <section class="content">

        <div class="row">
                <div class="col-md-12">
                  <!-- Custom Tabs -->
                  <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#main" data-toggle="tab"><b>HOME</b></a></li>
                      <li><a href="#tab_1" data-toggle="tab"><b>DUKCAPIL</b></a></li>
                      <li><a href="#tab_2" data-toggle="tab"><b>KIA</b></a></li>
                      <li><a href="#tab_3" data-toggle="tab"><b>VISI MISI</b></a></li>
                    </ul>
                    <div class="tab-content">

                        <?php
                         if ($salah) { ?> <font color="red">
                      Password atau Username salah </font>
                          <?php }
                          if ($daftarsukses==1) { ?>
                       <font color="green" >Pendaftaran Berhasil, silahkan Login </font>
                           <?php }
                      if ($emaildouble || $nikdouble ) { echo " Pendaftaran Gagal Dikarenakan "; }
                     if ($emaildouble) { echo " email telah digunakan"; if ($nikdouble) { echo " dan "; } }
                    if ($nikdouble) { echo "NIK telah digunakan </br>"; } ?>
                    <div class="tab-pane active" id="main">
                      <div class="col-md-1"> </div>
                      <div class="col-md-4"> <br> <br>  <img src="<?php echo base_url(); ?>assets/dukcapil-logo.png" alt="Dukcapil" height="400"> </div>
                      <div class="col-md-6"> <br> <br><br> <br> <br><font face="arial" size="250PX" color = "blue"> <b>SISTEM INFORMASI KEPENDUDUKAN </b> </font> <br>
                                              <font face="Arial" size="250PX" color = "blue"> <b>KARTU IDENTITAS ANAK </b> </font> <br>
                                              <font face="arial" size="250PX" color = "blue"> <b>KOTA MAKASSAR </b> </font> <br>

                      </div>

                    </div>

                      <div class="tab-pane" id="tab_1">
                        <div class="callout callout-info">
                        <h2>Dinas Kependudukan dan Pencatatan Sipil</h2>
                        <h3><font color = "black">
                        <p>Berdasarkan Peraturan Daerah Kota Makassar Nomor 9 Tahun 2009 tentang penyelenggaraan Administrasi kependudukan dan
                        catatan sipil Kota Makassar,
                        bahwa penyelenggaraan pelayanan dokumen kependudukan merupakan kewenangan dinas kependudukan dan pencatatan sipil .
                        Berdasarkan kewenangan tersebut dapat diberikan gambaran umum pelayanan yang diberikan
                        Dinas Kependudukan dan Pencatatan Sipil meliputi : </p>

                        <br><p><b> 1. Pencatatan biodata penduduk : </b>
                        	Dapat dilakukan secara aktif oleh penduduk atas setiap peristiwa yang dialami baik peristiwa kependudukan maupun
                        	peristiwa yang dialami baik peristiwa kependudukan maupun peristiwa penting, atau sebaiknya pemerintah kota melalui
                        	Dinas dapat pula melakukan secara aktif </p>

                      <br><p><b>  2. Penerbitan pencatatan dokumen kependudukaan meliputi : </b>

                            <br>Penerbitan Kartu Tanda Penduduk (KTP)
                            <br>Penerbitan Kartu Keluarga (KK)
                            <br>Penerbitan Surat Pindah
                          </p>

                    <br><p><b>   3. Penerbitan dokumen pencatatan sipil meliputi : </b>

                            <br>Akta kelahiran
                            <br>Akta kematian
                            <br>Akta perkawinan
                            <br>Akta perceraian
                          </p>

                    <br><p> <b>   4. Perubahan akta pencatatan sipil meliputi :</b>

                            <br>Pengangkatan anak
                            <br>Pengesahan anak
                            <br>Perubahan nama
                          </p>
                      <br><p>  Perubahan Undang-Undang Nomor 23 Tahun 2006 tentang Administrasi Kependudukan dengan Undang-Undang Nomor 24 Tahun 2013 yang telah disahkan oleh DPR RI pada tanggal 26 November 2013 merupakan perubahan yang mendasar di bidang administrasi kependudukan. Tujuan utama dari perubahan UndangUndang dimaksud adalah untuk meningkatkan efektivitas pelayanan administrasi kependudukan kepada masyarakat, menjamin akurasi data kependudukan dan ketunggalan Nomor Induk Kependudukan (NIK) serta ketunggalan dokumen kependudukan. </p>
                    </h3> </font>
                      </div>
                      </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_2">
                        <div class="callout callout-info">

                          <h2>Kartu Identitas Anak</h2>
                          <font color = "black">
                          <p><h4> "Adalah bukti identitas resmi untuk anak di bawah 17 tahun yang berlaku selayaknya KTP
                            untuk orang dewasa pada umumnya.
                            Kartu ini diterbitkan oleh Dinas Kependudukan dan Pencatatan Sipil (Dukcapil) Kabupaten/Kota,
                            juga sama seperti KTP."</p>
                            <br> Sejak dikeluarkannya kebijakan KIA
                            lewat Peraturan Kementerian Dalam Negeri (Permendagri) No. 2 tahun 2016,
                            program pembuatan dan kepemilikan kartu identitas anak sudah mulai berlaku secara nasional.
                            <p>
                            <br> KIA diterbitkan dalam dua versi, yaitu untuk anak usia 0-5 tahun dan anak usia 5-17 tahun,
                            Masa berlaku kartu ini ternyata juga berbeda.
                            Masa berlaku KIA bagi anak usia kurang dari 5 tahun akan habis ketika usia mereka menginjak 5 tahun,
                            Sementara bagi anak usia di atas 5 tahun, maka masa berlakunya akan habis sampai anak berusia 17 tahun
                            kurang satu hari.</p>

                            <br><p> Kemudian ketika anak Anda berulang tahun yang ke-17,
                            KIA akan secara otomatis diubah menjadi KTP.
                            Hal ini karena nomor yang tertera di KIA akan sama dengan yang ada di KTP. </p>

                            <br><p>Secara umum, KIA memiliki kegunaan yang sama dengan KTP.
                            Menurut Permendagri nomor 2 tahun 2016, penerbitan KIA dapat melindungi pemenuhan hak anak,
                            menjamin akses sarana umum, hingga untuk mencegah terjadinya perdagangan anak.
                            Kartu ini juga dapat menjadi bukti identifikasi diri ketika sewaktu-waktu mengalami peristiwa buruk.</p>

                            <br><p>Tak hanya itu. KIA juga berguna untuk memudahkan anak mendapatkan pelayanan publik di bidang kesehatan,
                            pendidikan, imigrasi, perbankan, dan transportasi.</p>

                            <br><p>Mengutip Kompas.com, anak yang memiliki KIA bahkan dapat memperoleh diskon khusus di pusat perbelanjaan.
                            Namun, pemberian diskon hanya berlaku di tok-toko atau tempat belanja yang memang sudah menjadi mitra
                            pemerintah daerah. Jadi, berbagai kemudahan yang diberikan bagi pemegang KIA ini akan tergantung pada
                            masing-masing daerah. </p>
                        </h4>
                      </font>
                        </div>

                      </div>
                      <!-- /.tab-pane -->
                      <div class="tab-pane" id="tab_3">
                        <div class="callout callout-info">
                          <h2>Visi Misi</h2>
                          <font color = "black">
                          <p><h2>Visi & Misi
                            Dinas Kependudukan Dan Pencatatan Sipil ; </h2>
                            <br>
                            <br>
                            <br>


                            <b><h3>Visi</b></h3>
                            <h4>
                            <i> ” MAKASSAR MENUJU TERTIB KEPEMILIKAN DOKUMEN KEPENDUDUKAN DAN PENCATATAN SIPIL TAHUN 2019 ” </i></h4>
                            <br>
                            <br>
                            <br>

                              <b><h3>Misi</b></h3>
                            <h4>
                            Misi Dinas Kependuukan dan Catatan Sipil Kota Makassar sebagai berikut :

                            <br>Menyelenggarakan Administrasi Pendaftaran Penduduk dan Pencatatan Sipil secara terintegrasi melalui SIAK
                            <br>Meningkatkan Pengelolaan Database Kependudukan secara berkelanjutan
                            <br>Meningkatkan Sumberdaya yang professional secara berkelanjutan
                            <br>Menambah dan mengembangkan sarana dan prasarana SIAK secara berkelanjutan.
                            <br>Meningkatkan insentitas kajian kebijakan dan pengendalian administrasi Kependudukan dan Pencatatan Sipil
                            <br>Meningkatkan insentitas koordinasi dan sinkronisasi dengan instansi terkait dalam pelaksanaan tugas
                          </h4>

                          </p>
                        </font>
                        <div>


                      </div>
                      <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                  </div>
                  <!-- nav-tabs-custom -->
                </div>
                <!-- /.col -->


                <!-- /.col -->
              </div>

        <!-- /.box -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>TAHUN</b> 2019
      </div>
      <strong> SISTEM INFORMASI KEPENDUDUKAN KARTU IDENTITAS ANAK KOTA MAKASSAR</strong>
    </div>
    <!-- /.container -->
  </footer>
</div>
<div class="modal  fade" id="modal-info">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title">Daftar User Baru</h4>

       </div>
       <div class="modal-body">
         <form action="#" method="POST" class="form-horizontal textcol-sm-6">
         <div class="col-sm-12 text-center">
             <div class="form-group ">
                         <label for="NIK" class="col-sm-3 control-label">NIK</label>
                         <div class="col-sm-7">
                         <input type="text" class="form-control" id="NIK" name="NIK" placeholder="NIK" required>
                       </div>
             </div>
             <input type="hidden" id="custId" name="id" value="2">
             <div class="form-group">
                         <label for="nama" class="col-sm-3 control-label ">Nama</label>
                         <div class="col-sm-7">
                         <input type="text" class="form-control" id="nama" name="nama" placeholder="nama" required>
                       </div>
             </div>

             <div class="form-group">
                         <label for="email" class="col-sm-3 control-label">Email</label>
                         <div class="col-sm-7">
                         <input type="email" class="form-control" id="email" name="email" placeholder="email" required>
                       </div>
             </div>
             <div class="form-group">
                         <label for="password" class="col-sm-3 control-label">Password</label>
                         <div class="col-sm-7">
                         <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                       </div>
             </div>
             <div class="form-group">
                         <label for="confirm_password" class="col-sm-3 control-label">Retype Password</label>
                         <div class="col-sm-7">
                         <input type="password" class="form-control" id="confirm_password" placeholder="Retype Password">
                       </div>
             </div>
             <div class="form-group">
                         <label for="tempatlahir" class="col-sm-3 control-label">Tempat Lahir</label>
                         <div class="col-sm-7">
                         <input type="text" class="form-control" id="tempatlahir" name="tempatlahir" placeholder="Tempat Lahir" required>
                       </div>
             </div>



             <div class="form-group">
               <label for="datepicker" class="col-sm-3 control-label ">Tanggal Lahir</label>

                 <div class="input-group date  col-sm-7">
                     <div class="input-group-addon">
                     <i class="fa fa-calendar"></i>
                     </div>
                   <input type="text" name="tgllahir" class="form-control  pull-right" id="datepicker">
                 </div>
                       <!-- /.input group -->
               </div>

             <div class="form-group">
                         <label for="telpon" class="col-sm-3 control-label">No telpon</label>
                         <div class="col-sm-7">
                         <input type="text" class="form-control" id="telpon" name="telpon" placeholder="No Telpon" required>
                       </div>
             </div>
             <div class="form-group">
                         <label for="jk" class="col-sm-3 control-label">Jenis Kelamin</label>
                         <div class="col-sm-7">
                         <select class="form-control" name="jeniskelamin" required>
                           <option value="pria">Pria</option>
                           <option value="wanita">Wanita</option>
                         </select>
                       </div>
                       </div>
             <div class="row">
               <!-- /.col -->
               <!-- /.col -->
             </div>
           </div>


       </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="Submit" class="btn btn-primary">Daftar</button>
         </div>
       </form>
     </div>
     <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
 </div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>

<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>

<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url(); ?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->

<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
<script type="text/javascript">
                var password = document.getElementById("password");
                var confirm_password = document.getElementById("confirm_password");

                function validatePassword(){
                  if(password.value != confirm_password.value) {
                    document.getElementById("Button").disabled = true;
                  } else {
                  document.getElementById("Button").disabled = false;
                  }
                }

                password.onchange = validatePassword;
                confirm_password.onkeyup = validatePassword;
</script>
</body>
</html>
