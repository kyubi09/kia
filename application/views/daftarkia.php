
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script>
        	$(document).ready(function(){
	            $("#provinsi").change(function (){
	                var url = "<?php echo site_url('wilayah/add_ajax_kab');?>/"+$(this).val();
	                $('#kabupaten').load(url);
	                return false;
	            })

	   			$("#kabupaten").change(function (){
	                var url = "<?php echo site_url('wilayah/add_ajax_kec');?>/"+$(this).val();
	                $('#kecamatan').load(url);
	                return false;
	            })

	   			$("#kecamatan").change(function (){
	                var url = "<?php echo site_url('wilayah/add_ajax_des');?>/"+$(this).val();
	                $('#desa').load(url);
	                return false;
	            })
	        });
    	</script>
    <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <!-- Main content -->
    <section class="content" style="overflow:scroll">
      <div class="col-md-12">

        <h1>KOTA MAKASSAR <small>SISTEM INFORMASI ADMINISTRASI KEPENDUDUKAN (SIAK)</small> </h1>

        <div class="box pad bg-gray disabled color-palette">
          <div class="btn-group">
            <button type="button" class="btn btn-info">
              KIA
              <span class="caret"></span>
            </button>

          </div>


        </div>

        <div class="box">
          <div class="box-header with-border bg-light-blue color-palette">
            <h3 class="box-title">DAFTAR/CETAK KARTU IDENTITAS ANAK</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->

            <form class="form-horizontal" action="#" method="POST">
            <!-- right column -->
            <div class="col-md-6">
              <!-- Horizontal Form -->
                <!-- form start -->
                  <div class="box-body">
                    <div class="form-group">
                      <label for="provinsi" class="col-sm-3">Provinsi</label>

                      <div class="col-sm-9">
                        <select name="prov" class="form-control" id="provinsi">
			                       <option>- Select Provinsi -</option>
			                          <?php
				                            foreach($provinsi as $prov)
				                                {
					                                     echo '<option value="'.$prov->id.'">'.$prov->nama.'</option>';
				                                         }
			                                              ?>
		                    </select>
                      </div>
                    </div>
                  </div>

                  <div class="box-body">
                    <div class="form-group">
                      <label for="kecamatan" class="col-sm-3">Kecamatan</label>

                      <div class="col-sm-9">
                        <select name="kec" class="form-control" id="kecamatan">
                          <option value=''>Select Kecamatan</option>
                        </select>
                      </div>
                    </div>
                  </div>

                <!-- /.box-body -->
              </div>

              <div class="col-md-6">
                <!-- Horizontal Form -->
                  <!-- form start -->
                    <div class="box-body">
                      <div class="form-group">
                        <label for="kota" class="col-sm-3">Kabupaten/Kota</label>

                        <div class="col-sm-9">
                          <select name="kab" class="form-control" id="kabupaten">
			                         <option value=''>Select Kabupaten</option>
		                           </select>
                        </div>
                      </div>
                    </div>

                    <div class="box-body">
                      <div class="form-group">
                        <label for="desa" class="col-sm-3">Desa/Kelurahan</label>

                        <div class="col-sm-9">
                          <select name="desa" class="form-control" id="desa">
                            <option value=''>Select Desa</option>
                          </select>
                        </div>
                      </div>
                    </div>

                  <!-- /.box-body -->
                </div>

                <div class="">
                  <p class="box-title text-center" >Cari Berdasarkan</p>
                </div>

                <div class="col-md-6">
                  <!-- Horizontal Form -->
                    <!-- form start -->
                      <div class="box-body">
                        <div class="form-group">
                          <label for="nik" class="col-sm-3">NIK</label>

                          <div class="col-sm-9">
                            <input type="number" name="NIK" class="form-control" id="nik">
                          </div>
                        </div>
                      </div>

                      <div class="box-body">
                        <div class="form-group">
                          <label for="nama_lengkap" class="col-sm-3">Nama Lengkap</label>

                          <div class="col-sm-9">
                            <input type="text" name="nama" class="form-control" id="nama_lengkap" placeholder="Nama Lengkap">
                          </div>
                        </div>
                      </div>

                      <div class="box-body">
                        <div class="form-group">
                          <label for="akta_lahir" class="col-sm-3">Ada Akta Lahir</label>

                          <div class="col-sm-9">
                            <select name="akta" class="form-control" id="akta_lahir">
                              <option value="1">Ada</option>
                              <option value='0'>Tidak Ada</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    <!-- /.box-body -->
                  </div>

                  <div class="col-md-6">
                    <!-- Horizontal Form -->
                      <!-- form start -->
                        <div class="box-body">
                          <div class="form-group">
                            <label for="no_kk" class="col-sm-3">No. KK</label>

                            <div class="col-sm-9">
                              <input type="number" name="KK" class="form-control" id="no_kk">
                            </div>
                          </div>
                        </div>

                        <div class="box-body">
                          <div class="form-group">
                            <label for="tanggal_lahir"  class="col-sm-3">Tanggal Lahir</label>

                            <div class="col-sm-9">
                              <input type="date" name="tanggallahir" class="form-control" id="tanggal_lahir">
                            </div>
                          </div>
                        </div>

                        <div class="box-body">
                          <div class="form-group">
                            <label for="tanggal_entri"  class="col-sm-3">Tanggal Entri</label>

                            <div class="col-sm-9">
                              <input type="date" name="tanggalentri" class="form-control" id="tanggal_entri">
                            </div>
                          </div>
                        </div>
                      <!-- /.box-body -->
                    </div>

            <div class="box-footer">
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
            </div>
          </form>

        </div>
        <?php if ($search==1) {
          if ($hasil) { ?>
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nomor KK</th>
                  <th>NIK</th>
                  <th>Nomor Akta</th>
                  <th>Nama Lengkap</th>
                  <th>Tempat Tanggal Lahir</th>
                  <th>Photo</th>
                  <th>Operasi</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($hasil as $item) {
                ?>
                <tr>
                  <td><?php echo $item['nomor_kk']; ?></td>
                  <td><?php echo $item['nomor_nik']; ?></td>
                  <td><?php echo $item['no_akta_kelahiran']; ?></td>
                  <td><?php echo $item['nama_anak']; ?></td>
                  <td><?php echo $item['tempat_lahir']." ".$item['tanggal_lahir']; ?></td>
                  <td><div class="col-md-12">
                    <?php  if ($item['photo'])
                    { $photo="upload/".$item['photo']; }
                    else { $photo="assets/dist/img/default.jpeg"; }?>
                  <img src="<?php echo base_url().$photo;?>" width="100px"  class=" text-center">
                </div></td>
                  <td>
                    <?php if (!$item['no_akta_kelahiran']=="" && $item['registrasi']<1)
                            { ?> <a type="button" href="<?php echo base_url(); ?>admin/registrasi/<?php echo $item['nomor_nik']; ?>" class="btn btn-block btn-info btn-xs">Registrasi</a> <?php }
                          else {
                                if ($item['registrasi']==1) { ?>  <a type="button" href="<?php echo base_url(); ?>admin/unregis/<?php echo $item['nomor_nik']; ?>" class="btn btn-block btn-info btn-xs">Unregistrasi</a> <?php }
                                else { ?> Belum Bisa di Registrasi <?php }
                              } ?>
                      <a class="btn btn-block" href="<?php echo base_url(); ?>admin/edit/<?php echo $item['nomor_nik']; ?>">
                        <i class="fa fa-edit"></i> Edit

                  </td>

                </tr>
              <?php } ?>
                </tbody>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
        <?php } }?>

      </div>
        <!-- /.col -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
