<?php //echo var_dump($user); ?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <font size="2" color="black">
    <div class='col-md-4 col-sm-5 col-xs-6'>
      <div class="col-md-12 col-sm-12 col-xs-12 text-center"> PROVINSI SULAWESI SELATAN </div>
      <div class="col-md-12 col-sm-12 col-xs-12 text-center"> PEMERINTAH KOTA MAKASSAR </div>
      <table class="col-md-12 col-sm-12 col-xs-12">
        <tr>
        <td width="20%"> NIK </td>
        <td width="80%"> : <?php echo $user['0']['nomor_nik']; ?> </td>
        </tr>
      </table>
      <table class="col-md-12 col-sm-12 col-xs-12" >
        <tr>
        <td width="25%"> Nama Lengkap </td>
        <td width="50"> : <?php echo $user['0']['nama_anak']; ?> </td>
        <td width=25% rowspan="9"> <?php  if ($user[0]['photo'])
        { $photo="upload/".$user[0]['photo']; }
        else { $photo="assets/dist/img/default.jpeg"; }?>
      <img src="<?php echo base_url().$photo;?>" width="100px" height="100px"  class=" text-center"> </td>
        </tr>
        <tr>
        <td width="25%"> Tempat/Tgl Lahir </td>
        <td width="50"> : <?php $tl = date("d-M-Y", strtotime($user['0']['tanggal_lahir']));
        echo $user['0']['tempat_lahir'].",".$tl; ?> </td>
        </tr>
        <tr>
        <td width="25%"> Jenis Kelamin </td>
        <td width="50"> <table>
                        <tr>
                          <td width="60%">: <?php echo $user['0']['jenis_kelamin']; ?> </td>
                          <td width="30%"> Gol. Darah : <?php echo $user['0']['goldar']; ?> </td>
                        </tr>
                        </table>
        </td>
        </tr>
        <tr>
        <td width="25%"> Nomor Kartu Keluarga </td>
        <td width="50"> : <?php echo $user['0']['nomor_kk']; ?> </td>
        </tr>
        <tr>
        <td width="25%"> Nama Kepala Keluarga </td>
        <td width="50"> : <?php echo $user['0']['nama_ayah']; ?> </td>
        </tr>
        <tr>
        <td width="25%"> Nomor Akta Kelahiran </td>
        <td width="50"> : <?php echo $user['0']['no_akta_kelahiran']; ?> </td>
        </tr>
        <tr>
        <td width="25%"> Agama </td>
        <td width="50"> : <?php echo $user['0']['agama']; ?> </td>
        </tr>
        <tr>
        <td width="25%"> Kewarganegaraan </td>
        <td width="50"> : WNI </td>
        </tr>
        <tr>
        <td width="25%"> Alamat </td>
        <td width="50"> : <?php echo $user['0']['alamat']; ?> </td>
        </tr>
      </table>
      <div class="col-md-1 col-sm-1 col-xs-1"> </div>
      <div class="col-md-5 col-sm-5 col-xs-5">
      </font>
      <font size="1" color="black">
      <table class="col-md-12 col-sm-12 col-xs-12" >
        <tr>
          <td width="50%"> RT/RW </td>
          <td width="50%"> : <?php echo $user['0']['rt']; ?>/<?php echo $user['0']['rw']; ?> </td>
        </tr>
        <tr>
          <td width="50%"> Desa/Kelurahan </td>
          <td width="50%"> : <?php echo $user['0']['kelurahan']; ?> </td>
        </tr>
        <tr>
          <td width="50%"> Kecamatan </td>
          <td width="50%"> : <?php echo $user['0']['kecamatan']; ?> </td>
        </tr>
        <tr>
          <td width="50%"> </td>
          <td width="50%"> </td>
        </tr>
        <tr>
          <td width="50%"> Berlaku s/d </td>
          <td width="50%"> : 24-11-2029 </td>
        </tr>
      </table>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-6">
        KOta MAKASSAR, 29-05-2019 <br>
        Kepala Dinas Kependudukan dan Pencatatan Sipil<br><br>
        NAMNYA<br>
        NIP. 19670303 199403 2 001

      </div>
    </div>
    </font>


  </section>

  <!-- Main content -->

  <!-- /.content -->
</div>
