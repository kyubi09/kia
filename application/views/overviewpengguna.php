
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <!-- Main content -->
    <section class="content" style="overflow:scroll">
      <div class="row">
        <div class="col-md-12">
          <h1>KOTA MAKASSAR <small>SISTEM INFORMASI ADMINISTRASI KEPENDUDUKAN (SIAK)</small> </h1>

          <div class="box pad bg-gray disabled color-palette">
            <h3>INFORMASI PENGGUNA</h3>
          </div>

          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#overview" data-toggle="tab">Overview</a></li>
              <li><a href="#pengguna" data-toggle="tab">Timeline</a></li>
              <li><a href="#pasal" data-toggle="tab">Pasal</a></li>
              <li><a href="#aktivitas" data-toggle="tab">Aktivitas</a></li>
              <li><a href="#versi" data-toggle="tab">Versi</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="overview">

                <div class="row">
                  <div class="col-sm-3">
                    <div class="box">
                      <div class="box-body"  style="height:340px;background-color:grey;">
                        <img class="img-responsive" src="../../dist/img/photo2.png" alt="Photo">
                      </div>
                    </div>
                  </div>

                  <!-- /.col -->
                  <div class="col-sm-8">
                    <h4>NAMA ORANG</h4>
                    <p class="text-aqua"> <strong>Selamat Datang Di Sistem Informasi Administrasi Kependudukan (SIAK) </strong> </p>
                    <p>Pengoperasian Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <div class="box box-default">
                      <div class="box-body">
                        <div class="callout callout-warning">
                          <h4>TIDAK ADA INFORMASI BARU</h4>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.col -->
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="box">
                      <div class="box-header with-border bg-light-blue color-palette">
                        <h3 class="box-title">Sistem Informasi Administrasi Kependudukan (SIAK)</h3>
                      </div>
                      <div class="box-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                      </div>

                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="box">

                      <div class="box-header with-border bg-yellow color-palette">
                        <h3 class="box-title">Untuk Menjadi Perhatian...!!!</h3>
                      </div>
                      <div class="box-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                      </div>
                    </div>
                  </div>
                </div>



              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="pengguna">
                <div class="row">
                  <div class="col-md-3">
                    <div class="box box-solid">

                      <div class="box-body no-padding">
                        <ul class="nav nav-pills nav-stacked">
                          <li class="active"> <a href="#"><i class="fa fa-user"></i> Data Pribadi</a></li>
                          <li>                <a href="#"><i class="fa fa-iamge"></i> Ganti Foto</a></li>
                          <li>                <a href="#"><i class="fa fa-lock-alt"></i> Ganti Kata Kunci</a></li>
                          <li>                <a href="#"><i class="fa fa-cog"></i> Pengaturan</a></li>
                        </ul>
                      </div>
                        <!-- /.box-body -->
                      </div>
                    </div>

                    <div class="col-md-9">
                      <div class="box">
                        <div class="box-header">
                          <h3 class="box-title">DATA PENGGUNA</h3>
                        </div>
                        <div class="box-body">
                          <table class="table">
                            <tr>
                              <td style="width:200px">Id Pengguna</td>
                              <td>: <strong>ATI</strong> </td>
                            </tr>

                            <tr>
                              <td style="width:200px">Nama Lengkap</td>
                              <td>: <strong>YEET</strong> </td>
                            </tr>

                            <tr>
                              <td style="width:200px">NIP</td>
                              <td>: <strong>YEET</strong> </td>
                            </tr>

                            <tr>
                              <td style="width:200px">Tempat Lahir</td>
                              <td>: <strong>YEET</strong> </td>
                            </tr>

                            <tr>
                              <td style="width:200px">Tanggal Lahir</td>
                              <td>: <strong>YEET</strong> </td>
                            </tr>

                            <tr>
                              <td style="width:200px">Jenis Kelamin</td>
                              <td>: <strong>YEET</strong> </td>
                            </tr>

                            <tr>
                              <td style="width:200px">Telephone/Handphone/td>
                              <td>: <strong>YEET</strong> </td>
                            </tr>

                            <tr>
                              <td style="width:200px">Golongan</td>
                              <td>: <strong>YEET</strong> </td>
                            </tr>

                            <tr>
                              <td style="width:200px">Pangkat</td>
                              <td>: <strong>YEET</strong> </td>
                            </tr>

                            <tr>
                              <td style="width:200px">Jabatan</td>
                              <td>: <strong>YEET</strong> </td>
                            </tr>

                            <tr>
                              <td style="width:200px">Nama Kantor</td>
                              <td>: <strong>YEET</strong> </td>
                            </tr>

                            <tr>
                              <td style="width:200px">Alamat Kantor</td>
                              <td>: <strong>YEET</strong> </td>
                            </tr>

                            <tr>
                              <td style="width:200px">Abaikan IP Adress</td>
                              <td>: <strong>YEET</strong> </td>
                            </tr>

                            <tr>
                              <td style="width:200px">Kelompok Pengguna</td>
                              <td>: <strong>YEET</strong> </td>
                            </tr>
                          </table>
                        </div>

                        <div class="box-header">
                          <h3 class="box-title">WILAYAH</h3>
                        </div>

                        <div class="box-body">
                          <table class="table">
                            <tr>
                              <td style="width:200px">Provinsi</td>
                              <td>: <strong>ATI</strong> </td>
                            </tr>

                            <tr>
                              <td style="width:200px">Kabupaten/Kota</td>
                              <td>: <strong>ATI</strong> </td>
                            </tr>
                          </table>
                        </div>
                      </div>
                    </div>
                </div>
              </div>

              <!-- /.tab-pane -->

              <div class="tab-pane" id="pasal">

              </div>

              <div class="tab-pane" id="aktivitas">

              </div>

              <div class="tab-pane" id="versi">

              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>

        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
