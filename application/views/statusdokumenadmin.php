<div class="content-wrapper">

  <section class="content">
    <div class="col-md-12 ">
      <div class="box-header with-border text-center">
          <h2 class="box-title text-center">Status Dokumen</h2>
      </div>
      <form action="#" role="form" method="post">
      <div class="box-body col-md-12">
        <div class="form-group col-md-4">
                <label>Mulai:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="mulai" class="form-control pull-right" id="datepicker">
                </div>
        </div>
            <div class="form-group col-md-4">
                    <label>Sampai:</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="selesai" class="form-control pull-right" id="datepicker2">
                    </div>
            </div>



      </div>
      <div class="col-md-8">
        <button type="Submit" class="btn btn-danger btn-xs pull-right">Submit</button>
      </div>
    </form>
      <div class="box-body col-md-12">
        <?php if ($hasil) { ?>
          <table class="table" border="2">
            <tr>
              <th>Nomor</th>
              <th>NIK</th>
              <th>Nama</th>
              <th>Nomor KK</th>
              <th>Waktu Daftar</th>
              <th>Waktu Pengambilan</th>
              <th>Foto</th>
              <th>Status Dokumen</th>
            </tr>
            <?php foreach ($hasil as $item) { ?>
            <tr>
              <tr>
                <td class="text-enter"><?php echo $item['nomor_antrian']; ?></td>
                <td><?php echo $item['nomor_nik']; ?></td>
                <td><?php echo $item['nama_anak']; ?></td>
                <td><?php echo $item['nomor_kartu_keluarga']; ?></td>
                <td>
                  <?php $tl=date("d-M-Y", strtotime($item['tanggal_daftar']));
                  echo $tl; ?> <br> <?php echo $item['jam']; ?></td>

                <td><?php
                $tl=date("d-M-Y", strtotime($item['tanggal_ambil']));
                echo $tl; ?>
              </td>
                <td><?php  if ($item['foto'])
                  { $photo="upload/".$item['foto']; }
                  else { $photo="assets/dist/img/default.jpeg"; }?>
                <img src="<?php echo base_url().$photo;?>" width="100px" height="100px"  class=" text-center">
                </td>
                <td><?php if ($item['statusdokumen']==1) { ?> Sudah Diregistrasi <?php }
                          if ($item['statusdokumen']==0) { ?> Registrasi Dibatalkan Oleh Admin <?php }
                          if ($item['statusdokumen']==2) { ?> Sudah Dicetak <?php }
                          if ($item['statusdokumen']==3) { ?> Sudah diambil <?php }
                          if ($item['statusdokumen']==4) { ?> Blangko Habis <?php }?>
                          <br> Diedit Oleh : <?php
                          echo $item['admin_edit'];
                          $tl=date("d-M-Y H:i:s", strtotime($item['waktu_edit']));
                           ?>
                          <br> Waktu : <?php echo $tl; ?>
                </td>
            </tr>
            <?php } ?>
          </table>
        <?php } ?>
      </div>

    </div>
  </section>

</div>
