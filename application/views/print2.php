<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">



  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body>


  <!-- Content Header (Page header) -->
  <font size="2" color="black">
  <div class='col-md-4 col-sm-5 col-xs-6'>
    <div class="col-md-12 col-sm-12 col-xs-12 text-center"> PROVINSI SULAWESI SELATAN </div>
    <div class="col-md-12 col-sm-12 col-xs-12 text-center"> PEMERINTAH KOTA MAKASSAR </div>
    <table class="col-md-12 col-sm-12 col-xs-12">
      <tr>
      <td width="20%"> NIK </td>
      <td width="80%"> : <?php echo $user['0']['nomor_nik']; ?> </td>
      </tr>
    </table>
    <table class="col-md-12 col-sm-12 col-xs-12" >
      <tr>
      <td width="25%"> Nama Lengkap </td>
      <td width="50"> : <?php echo $user['0']['nama_anak']; ?> </td>
      <td width=25% rowspan="9"><?php  if ($user[0]['photo'])
      { $photo="upload/".$user[0]['photo']; }
      else { $photo="assets/dist/img/default.jpeg"; }?>
    <img src="<?php echo base_url().$photo;?>" width="100px" height="100px"  class=" text-center"> </td>
      </tr>
      <tr>
      <td width="25%"> Tempat/Tgl Lahir </td>
      <td width="50"> : <?php $tl = date("d-M-Y", strtotime($user['0']['tanggal_lahir']));
      echo $user['0']['tempat_lahir'].",".$tl; ?> </td>
      </tr>
      <tr>
      <td width="25%"> Jenis Kelamin </td>
      <td width="50"> <table>
                      <tr>
                        <td width="60%">: <?php echo $user['0']['jenis_kelamin']; ?> </td>
                        <td width="30%"> Gol. Darah : <?php echo $user['0']['goldar']; ?> </td>
                      </tr>
                      </table>
      </td>
      </tr>
      <tr>
      <td width="25%"> Nomor Kartu Keluarga </td>
      <td width="50"> : <?php echo $user['0']['nomor_kk']; ?> </td>
      </tr>
      <tr>
      <td width="25%"> Nama Kepala Keluarga </td>
      <td width="50"> : <?php echo $user['0']['nama_ayah']; ?> </td>
      </tr>
      <tr>
      <td width="25%"> Nomor Akta Kelahiran </td>
      <td width="50"> : <?php echo $user['0']['no_akta_kelahiran']; ?> </td>
      </tr>
      <tr>
      <td width="25%"> Agama </td>
      <td width="50"> : <?php echo $user['0']['agama']; ?> </td>
      </tr>
      <tr>
      <td width="25%"> Kewarganegaraan </td>
      <td width="50"> : WNI </td>
      </tr>
      <tr>
      <td width="25%"> Alamat </td>
      <td width="50"> : <?php echo $user['0']['alamat']; ?> </td>
      </tr>
    </table>
    <div class="col-md-1 col-sm-1 col-xs-1"> </div>
    <div class="col-md-5 col-sm-5 col-xs-5">
    </font>
    <font size="1" color="black">
    <table class="col-md-12 col-sm-12 col-xs-12" >
      <tr>
        <td width="50%"> RT/RW </td>
        <td width="50%"> : <?php echo $user['0']['rt']; ?>/<?php echo $user['0']['rw']; ?> </td>
      </tr>
      <tr>
        <td width="50%"> Desa/Kelurahan </td>
        <td width="50%"> : <?php echo $user['0']['kelurahan']; ?> </td>
      </tr>
      <tr>
        <td width="50%"> Kecamatan </td>
        <td width="50%"> : <?php echo $user['0']['kecamatan']; ?> </td>
      </tr>
      <tr>
        <td width="50%"> </td>
        <td width="50%"> </td>
      </tr>
      <tr>
        <td width="50%"> Berlaku s/d </td>
        <td width="50%"> : 24-11-2029 </td>
      </tr>
    </table>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
      KOta MAKASSAR, 29-05-2019 <br>
      Kepala Dinas Kependudukan dan Pencatatan Sipil<br><br>
      NAMNYA<br>
      NIP. 19670303 199403 2 001

    </div>
  </div>
  </font>


  <!-- Main content -->

  <!-- /.content -->

  <footer class="main-footer">

  </footer>


<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/dist/js/adminlte.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap  -->
<script src="<?php echo base_url();?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url();?>assets/bower_components/chart.js/Chart.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url();?>assets/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>

<!-- DataTables -->
<script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
