
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <section class="content">
      <div class="col-md-12">


        <div class="box box-primary">
          <div class="">
            <h3 class="box-title text-center">Status Dokumen</h3>
          </div>
        <div class="box-body col-md-12">
          <?php
            if ($hasil) { ?>

              <table class="table" border="2">
                <tr>
                  <th>Nomor Antrian</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Nomor KK</th>
                  <th>Waktu Daftar</th>
                  <th>Waktu Pengambilan</th>
                  <th>Foto</th>
                  <th>Status Dokumen</th>
                </tr>
                <?php foreach ($hasil as $item) {
                ?>
                <tr>
                  <td><?php echo $item['nomor_antrian']; ?></td>
                  <td><?php echo $item['nomor_nik']; ?></td>
                  <td><?php echo $item['nama_anak']; ?></td>
                  <td><?php echo $item['nomor_kartu_keluarga']; ?></td>
                  <td>
                    <?php $tl=date("d-M-Y H:i:s", strtotime($item['tanggal_daftar']));
                    echo $tl; ?></td>

                  <td><?php
                  $tl=date("d-M-Y", strtotime($item['tanggal_ambil']));
                  echo $tl; ?>
                </td>
                  <td><?php  if ($item['foto'])
                    { $photo="upload/".$item['foto']; }
                    else { $photo="assets/dist/img/default.jpeg"; }?>
                  <img src="<?php echo base_url().$photo;?>" width="100px"  class=" text-center">
                  </td>
                  <td><?php if ($item['statusdokumen']==1) { ?> Sudah Diregistrasi <?php }
                            if ($item['statusdokumen']==0) { ?> Registrasi Dibatalkan Oleh Admin <?php }
                            if ($item['statusdokumen']==2) { ?> Sudah Dicetak <?php }
                            if ($item['statusdokumen']==3) { ?> Sudah diambil <?php }
                            if ($item['statusdokumen']==4) { ?> Blangko Habis <?php }
                            ?>
                  </td>

                </tr>
              <?php } ?>

              </table>

        <?php } ?>
        </div>
        </div>


      </div>
        <!-- /.col -->
    </section>


  </div>

  <!-- /.content-wrapper -->
