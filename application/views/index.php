
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>

    <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <!-- Main content -->
    <section class="content" style="overflow:scroll">
      <div class="col-md-12">

        <h1>KOTA MAKASSAR <small>SISTEM INFORMASI ADMINISTRASI KEPENDUDUKAN (SIAK)</small> </h1>

        <div class="box pad bg-gray disabled color-palette">
          <div class="btn-group">
            <button type="button" class="btn btn-info">
              KIA
              <span class="caret"></span>
            </button>
          </div>

        </div>

        <div class="box">
          <div class="box-header with-border bg-light-blue color-palette">
            <h3 class="box-title">PENDAFTARAN KARTU IDENTITAS ANAK</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->

            <form class="form-horizontal" action="#" method="POST">
            <!-- right column -->
                <div class="">
                  <p class="box-title text-center" >Cari Berdasarkan <?php if ($kosong==1)  { ?> <font color="red"> <br> Mohon Masukkan Salah Satu Kriteria Pencarian </font> <?php }?></p>

                </div>
                <div class="col-md-6">
                  <!-- Horizontal Form -->
                    <!-- form start -->
                      <div class="box-body">
                        <div class="form-group">
                          <label for="nik" class="col-sm-3 control-label">NIK</label>

                          <div class="col-sm-9">
                            <input type="text" minlength="16" maxlength="16" name="NIK" class="form-control" id="nik">
                          </div>
                        </div>
                      </div>

                      <div class="box-body">
                        <div class="form-group">
                          <label for="nama_lengkap" class="col-sm-3 control-label">Nama Lengkap</label>

                          <div class="col-sm-9">
                            <input type="text" name="nama" class="form-control" id="nama_lengkap" placeholder="Nama Lengkap">
                          </div>
                        </div>
                      </div>

                      <div class="box-body">
                        <div class="form-group">
                          <label for="akta_lahir" class="col-sm-3 control-label">Ada Akta Lahir</label>

                          <div class="col-sm-9">
                            <select name="akta" class="form-control" id="akta_lahir">
                              <option value="3">- Pilih -</option>
                              <option value="1">Ada</option>
                              <option value='0'>Tidak Ada</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    <!-- /.box-body -->
                  </div>

                  <div class="col-md-6">
                    <!-- Horizontal Form -->
                      <!-- form start -->
                        <div class="box-body">
                          <div class="form-group">
                            <label for="no_kk" class="col-sm-3 control-label">No. KK</label>

                            <div class="col-sm-9">
                              <input type="text" minlength="16" maxlength="16" name="KK" class="form-control" id="no_kk">
                            </div>
                          </div>
                        </div>

                        <div class="box-body">
                          <div class="form-group">
                            <label for="tanggal_lahir"  class="col-sm-3 control-label">Tanggal Lahir</label>

                            <div class="col-sm-9">
                              <input type="date" name="tanggallahir" class="form-control" id="tanggal_lahir">
                            </div>
                          </div>
                        </div>

                        <div class="box-body">
                          <div class="form-group">
                            <label for="tanggal_entri"  class="col-sm-3 control-label">Tanggal Entri</label>

                            <div class="col-sm-9">
                              <input type="date" name="tanggalentri" class="form-control" id="tanggal_entri">
                            </div>
                          </div>
                        </div>
                      <!-- /.box-body -->
                    </div>

            <div class="box-footer">
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
            </div>
          </form>

        </div>
        <?php if ($search==1) {
          if ($hasil) { ?>
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nomor KK</th>
                  <th>NIK</th>
                  <th>Nomor Akta</th>
                  <th>Nama Lengkap</th>
                  <th>Tempat Tanggal Lahir</th>
                  <th>Photo</th>
                  <th>Operasi</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($hasil as $item) {
                ?>
                <tr>
                  <td><?php echo $item['nomor_kk']; ?></td>
                  <td><?php echo $item['nomor_nik']; ?></td>
                  <td><?php echo $item['no_akta_kelahiran']; ?></td>
                  <td><?php echo $item['nama_anak']; ?></td>
                  <td><?php $tl=date("d-M-Y", strtotime($item['tanggal_lahir']));
                  echo $item['tempat_lahir']." ".$tl; ?></td>
                  <td><div class="col-md-12">
                    <?php  if ($item['photo'])
                    { $photo="upload/".$item['photo']; }
                    else { $photo="assets/dist/img/default.jpeg"; }?>
                  <img src="<?php echo base_url().$photo;?>" width="100px"  class=" text-center">
                </div></td>
                  <td>
                    <?php if (!$item['no_akta_kelahiran']=="" && $item['registrasi']<1)
                            { ?> <a type="button" href="<?php echo base_url(); ?>user/registrasi/<?php echo $item['nomor_nik']; ?>" class="btn btn-block btn-info btn-xs">Registrasi</a> <?php }
                          else { if ($item['registrasi']==1) { ?> Sudah Diregistrasi <?php }
                                  if ($item['registrasi']==0) { ?>
                                  Belum Bisa di Registrasi <?php } }
                               ?>
                      <a class="btn btn-block" href="<?php echo base_url(); ?>user/photo/<?php echo $item['nomor_nik']; ?>">
                        <i class="fa fa-edit"></i> Edit
                  </td>

                </tr>
              <?php } ?>
                </tbody>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
        <?php } }?>

      </div>
        <!-- /.col -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
