
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="col-md-12">

        <div class="box box-primary">
          <div class="">
            <h3 class="box-title text-center">Biodata Anak</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form">
            <div class="box-body col-md-12">

              <table class="table">
                <tr>
                  <td width="20%">NIK</td>
                  <td width="80%">:</td>
                </tr>
                <tr>
                  <td width="20%">Nama Lengkap</td>
                  <td width="80%">:</td>
                </tr>
                <tr>
                  <td width="20%">Tempat Lahir</td>
                  <td width="80%">:</td>
                </tr>
                <tr>
                  <td width="20%">Tanggal Lahir</td>
                  <td width="80%">:</td>
                </tr>
                <tr>
                  <td width="20%">Jenis Kelamin</td>
                  <td width="80%">:</td>
                </tr>
                <tr>
                  <td width="20%">Agama</td>
                  <td width="80%">:</td>
                </tr>
                <tr>
                  <td width="20%">Nama Ibu</td>
                  <td width="80%">:</td>
                </tr>
                <tr>
                  <td width="20%">Nama Ayah</td>
                  <td width="80%">:</td>
                </tr>
                <tr>
                  <td width="20%">No KK</td>
                  <td width="80%">:</td>
                </tr>
                <tr>
                  <td width="20%">Provinsi</td>
                  <td width="80%">:</td>
                </tr>
                <tr>
                  <td width="20%">Kecamatan</td>
                  <td width="80%">:</td>
                </tr>
                <tr>
                  <td width="20%">Alamat KK</td>
                  <td width="80%">:</td>
                </tr>
                <tr>

                  <td width="20%">No. Akta Kelahiran</td>
                  <td width="80%">: </td>
                </tr>
              </table>


            <div class="">
              <button type="submit" class="btn btn-primary text-center">Simpan</button>
            </div>
          </form>
        </div>



      </div>
        <!-- /.col -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
