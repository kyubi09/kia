
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <!-- Main content -->
    <section class="content" style="overflow:scroll">
      <div class="row">
        <div class="col-md-12">

          <h1>KOTA MAKASSAR <small>SISTEM INFORMASI ADMINISTRASI KEPENDUDUKAN (SIAK)</small> </h1>

          <div class="box pad bg-gray disabled color-palette">
            <div class="btn-group">
              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                KIA
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a href="#">Dropdown link</a></li>
                <li><a href="#">Dropdown link</a></li>
              </ul>
            </div>

            <div class="btn-group">
              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                Laporan
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a href="#">Dropdown link</a></li>
                <li><a href="#">Dropdown link</a></li>
              </ul>
            </div>

            <div class="btn-group">
              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                Lain-lain
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a href="#">Dropdown link</a></li>
                <li><a href="#">Dropdown link</a></li>
              </ul>
            </div>
          </div>

          <div class="box">
            <div class="box-header with-border bg-light-blue color-palette">
              <h3 class="box-title">UBAH DATA KARTU IDENTITAS ANAK</h3>
            </div>
            <div class="box-header with-border">
              <h3 class="box-title">BIODATA ANAK</h3>
            </div>

            <div class="box-body">
              <table class="table">
                <tr>
                  <td>NIK</td>
                  <td>: <strong>1345643245675645</strong></td>
                  <!-- 1 -->
                  <td>Nama Lengkap</td>
                  <td>: <strong>yeet</strong></td>
                </tr>

                <tr>
                  <td>Jenis Kelamin</td>
                  <td>: <strong>yeet</strong></td>
                  <!-- 2 -->
                  <td>Tempat/Tanggal lahir</td>
                  <td>: <strong>yeet</strong></td>
                </tr>

                <tr>
                  <td>Gol. Darah</td>
                  <td>: <strong>yeet</strong></td>
                  <!-- 3 -->
                  <td>Agam</td>
                  <td>: <strong>yeet</strong></td>
                </tr>

                <tr>
                  <td>NIK Ibu</td>
                  <td>: <strong>yeet</strong></td>
                  <!-- 4 -->
                  <td>Nama Ibu</td>
                  <td>: <strong>yeet</strong></td>
                </tr>

                <tr>
                  <td>NIK Ayah</td>
                  <td>: <strong>yeet</strong></td>
                  <!-- 5 -->
                  <td>Nama Ayah</td>
                  <td>: <strong>yeet</strong></td>
                </tr>

                <tr>
                  <td>No. KK</td>
                  <td>: <strong>yeet</strong></td>
                  <!-- 6 -->
                  <td></td>
                  <td><strong></strong></td>
                </tr>

                <tr>
                  <td>Provinsi</td>
                  <td>: <strong>yeet</strong></td>
                  <!-- 7 -->
                  <td>Kabupaten/Kota</td>
                  <td>: <strong>yeet</strong></td>
                </tr>

                <tr>
                  <td>Kecamatan</td>
                  <td>: <strong>yeet</strong></td>
                  <!-- 8 -->
                  <td>Desa/Kelurahan</td>
                  <td>: <strong>yeet</strong></td>
                </tr>

                <tr>
                  <td>Alamat KK</td>
                  <td>: <strong>yeet</strong></td>
                  <!-- 9 -->
                  <td></td>
                  <td></td>
                </tr>

                <tr>
                  <td>RT/RW</td>
                  <td>: <strong>yeet</strong></td>
                  <!-- 10 -->
                  <td></td>
                  <td></td>
                </tr>

                <tr>
                  <td>Dusun/Kampung/Dukuh</td>
                  <td>: <strong>yeet</strong></td>
                  <!-- 11 -->
                  <td>Kode Pos</td>
                  <td>: <strong>yeet</strong></td>
                </tr>

                <tr>
                  <td>No. Akta Kelahiran</td>
                  <td>: <strong>yeet</strong></td>
                  <!-- 12 -->
                  <td>Anak ke</td>
                  <td>: <strong>yeet</strong></td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <div class="box-tools pull-right">
                <div class="btn-group">
                  <button type="button" class="btn btn-info dropdown-toggle bg-green color-palette" data-toggle="dropdown">
                    <i class="fa fa-save"></i> Simpan
                  </button>
                </div>

                <div class="btn-group">
                  <button type="button" class="btn btn-info dropdown-toggle bg-green color-palette" data-toggle="dropdown">
                    <i class="fa fa-file-image"></i> Foto
                  </button>
                </div>

                <div class="btn-group">
                  <button type="button" class="btn btn-info dropdown-toggle bg-purple color-palette" data-toggle="dropdown">
                    <i class="fa fa-undo"></i> Kembali
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
