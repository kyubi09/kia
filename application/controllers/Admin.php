<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 public function __construct()
		 	{
			 parent::__construct();
			 $this->load->model('anak_model');
			 if ($this->session->userdata('userlevel')<>2)
			 {
				 redirect('login');
			 }

			}

	public function index()
	{
		$get_prov = $this->db->select('*')->from('wilayah_provinsi')->get();

		$data['provinsi'] = $get_prov->result();
		$data['form']=$this->input->post();
		$data['search']=0;
		if ($data['form'])
		{
			$data['search']=1;
				$prov=$this->input->post('prov');
				$idprov=$prov;
				if ($prov>0) {
					$prov=$this->anak_model->getprov($prov);
					$prov=$prov[0]['nama'];
				}
				$kab=$this->input->post('kab');
				$idkab=$kab;
				if ($kab>0) {
					$kab=$this->anak_model->getkab($kab);
					$kab=$kab[0]['nama'];
				}
				$kec=$this->input->post('kec');
				$idkec=$kec;
				if ($kec>0) {
					$kec=$this->anak_model->getkec($kec);
					$kec=$kec[0]['nama'];
				}
				$desa=$this->input->post('desa');
				$iddesa=$desa;
				if ($desa>0) {
				$desa=$this->anak_model->getdes($desa);
				$desa=$desa[0]['nama'];
				}
				$NIK=$this->input->post('NIK');
				$nama=$this->input->post('nama');
				$akta=$this->input->post('akta');
				$KK=$this->input->post('KK');
				$tanggallahir=$this->input->post('tanggallahir');
				$tanggalentri=$this->input->post('tanggalentri');

				$data['hasil']=$this->anak_model->search($idprov,$idkab,$idkec,$iddesa,$prov,$kab,$kec,$desa,$NIK,$nama,$akta,$KK,$tanggallahir,$tanggalentri);
				}
		$this->load->view('header');
		$this->load->view('cetak2',$data);
		$this->load->view('footer');
	}

	public function registrasi($NIK)
	{

		$data['regis']=$this->anak_model->getuser($NIK);
		$data['regis']=$data['regis'][0];
		//echo var_dump($data['regis']);
		$no_nik=$data['regis']['nomor_nik'];
		$no_kk=$data['regis']['nomor_kk'];
		$tanggal_daftar=date("Y-m-d");
		$date = DateTime::createFromFormat('Y-m-d', $tanggal_daftar);
		$date->modify('+3 day');
		$tanggal_ambil = $date->format('Y-m-d H:i:s');
		$max=$this->anak_model->maxantri();
		$nomor_antrian=$max[0]["nomor_antrian"]+1;
		$namaanak=$data['regis']['nama_anak'];
		$foto=$data['regis']['photo'];
		$result=$this->anak_model->userregis($NIK);
		$cek=$this->anak_model->cekdokumen($NIK);
		$admin=$this->session->userdata("nama");
		if (!$cek) {
		$data['forminput'] = array(
			'nomor_nik' => $NIK,
			'nomor_kartu_keluarga' => $no_kk,
			'tanggal_daftar' => $tanggal_daftar,
			'tanggal_ambil' => $tanggal_ambil,
			'nomor_antrian' => $nomor_antrian,
			'nama_anak' => $namaanak,
			'foto' => $foto,
			'admin_edit'=> $admin

			);
		$result=$this->anak_model->tambahdokumen($data['forminput']);
		}

		if ($cek) {
		$data['forminput'] = array(
			'nomor_kartu_keluarga' => $no_kk,
			'tanggal_daftar' => $tanggal_daftar,
			'tanggal_ambil' => $tanggal_ambil,
			'nomor_antrian' => $nomor_antrian,
			'nama_anak' => $namaanak,
			'foto' => $foto,
			'admin_edit'=> $admin

			);
		$result=$this->anak_model->updatedokumen($data['forminput'],$NIK);
		}
				redirect ('admin/daftar_kia');
	}

	public function unregis($NIK)
	{
		$result=$this->anak_model->unregis($NIK);
		redirect ('admin/daftar_kia');
	}

	public function photo($NIK)
	{
		$data['form']=$this->input->post();
		if ($data['form'])
		{
			//$NIK=$this->input->post('nik');
			$nama=$this->input->post('nama');
			$jenis_kelamin=$this->input->post('jenis_kelamin');
			$tempatlahir=$this->input->post('tempat_lahir');
			$tanggallahir=$this->input->post('tanggal_lahir');
			$tanggallahir=date("Y-m-d", strtotime($tanggallahir));
			$statuskawin=$this->input->post('statuskawin');
			$status_keluarga=$this->input->post('status_keluarga');
			$pendidikan=$this->input->post('pendidikan');
			if ($_FILES['photo']['error'] <> 4)
			{
				$config['upload_path']   = './upload/';
					     							$config['allowed_types'] = '*';
							 							$config['encrypt_name'] = true;
					     							//$this->load->library('upload', $config);
														$this->load->library('upload',$config);
													if ( !$this->upload->do_upload('photo'))
														{
															//echo var_dump($this->input->post());
															echo "gagal";
															$this->upload->display_errors();
															echo "<br>";
															echo var_dump($this->upload->data());
														}
														else
														{
															$upload_data=$this->upload->data();
															$file=$upload_data['file_name'];
															$data['forminput'] = array (
																'photo' => $file,
																'nama_anak' => $nama,
																'jenis_kelamin' => $jenis_kelamin,
																'tanggal_lahir' => $tanggallahir,
																'tempat_lahir' => $tempatlahir,
																'status_perkawinan' => $statuskawin,
																'status_keluarga' => $status_keluarga,
																'pendidikan' => $pendidikan,

															);
															$result=$this->anak_model->updateadmin($data,$NIK);

														}
			}
			if ($_FILES['photo']['error'] == 4)
			{

															$data['forminput'] = array (
																'nama_anak' => $nama,
																'jenis_kelamin' => $jenis_kelamin,
																'tanggal_lahir' => $tanggallahir,
																'tempat_lahir' => $tempatlahir,
																'status_perkawinan' => $statuskawin,
																'status_keluarga' => $status_keluarga,
																'pendidikan' => $pendidikan,

															);
															$result=$this->anak_model->updateadmin($data,$NIK);


			}
		}

		$data['user']=$this->anak_model->getuser($NIK);
		$this->load->view('header');
		$this->load->view('edit',$data);
		$this->load->view('footer');
	}

	public function edit($NIK)
	{
		$data['form']=$this->input->post();
		if ($data['form'])
		{
			//$NIK=$this->input->post('nik');
			$nomorakta=$this->input->post('aktalahir');

			if ($_FILES['photo']['error'] <> 4)
			{
				$config['upload_path']   = './upload/';
														$config['allowed_types'] = '*';
														$config['encrypt_name'] = true;
														//$this->load->library('upload', $config);
														$this->load->library('upload',$config);
													if ( !$this->upload->do_upload('photo'))
														{
															//echo var_dump($this->input->post());
															echo "gagal";
															$this->upload->display_errors();
															echo "<br>";
															echo var_dump($this->upload->data());
														}
														else
														{
															$upload_data=$this->upload->data();
															$file=$upload_data['file_name'];
															$data['forminput'] = array (
																'photo' => $file,
																'no_akta_kelahiran' => $nomorakta

															);
															$result=$this->anak_model->updateadmin($data,$NIK);

														}
			}
			if ($_FILES['photo']['error'] == 4)
			{

															$data['forminput'] = array (
																'no_akta_kelahiran' => $nomorakta,

															);
															$result=$this->anak_model->updateadmin($data,$NIK);


			}
		}

		$data['user']=$this->anak_model->getuser($NIK);
		$this->load->view('header');
		$this->load->view('edit2',$data);
		$this->load->view('footer');
	}

	public function addaktalahir()
	{
		$this->load->view('header');
		$this->load->view('addaktalahir');
		$this->load->view('footer');
	}



	public function cetak($NIK)
	{
		$data['user']=$this->anak_model->getuser($NIK);
		$this->load->view('header');
		$this->load->view('print',$data);
		$this->load->view('footer');


	}

	public function print($NIK)
	{
		$data['user']=$this->anak_model->getuser($NIK);
		$this->load->view('print2',$data);

		$html = $this->output->get_output();

		$this->load->library('pdf');
		$this->dompdf->loadHtml($html);
		$this->dompdf->setPaper('A4', 'landscape');
		$this->dompdf->render();
		$this->dompdf->stream("welcome.pdf", array("Attachment"=>0));

		// load view yang akan digenerate atau diconvert
	 //$html = $this->output->get_output();
	 // Load/panggil library dompdfnya
	 //$this->load->library('dompdf_gen');
	 // Convert to PDF
	 //$this->dompdf->load_html($html);
	 //$this->dompdf->render();
	 //utk menampilkan preview pdf
	 //$sekarang=date("d:F:Y:h:m:s");
	 //$this->dompdf->stream("pendaftaran".$sekarang.".pdf",array('Attachment'=>0));
	 //atau jika tidak ingin menampilkan (tanpa) preview di halaman browser
	 //$this->dompdf->stream("welcome.pdf");


	}

	public function overview()
	{
		$this->load->view('header');
		$this->load->view('overviewpengguna');
		$this->load->view('footer');
	}

	public function daftar_kia()
	{
		$get_prov = $this->db->select('*')->from('wilayah_provinsi')->get();

		$data['provinsi'] = $get_prov->result();
		$data['form']=$this->input->post();
		$data['search']=0;
		if ($data['form'])
		{
			$data['search']=1;
				$prov=$this->input->post('prov');
				$idprov=$prov;
				if ($prov>0) {
					$prov=$this->anak_model->getprov($prov);
					$prov=$prov[0]['nama'];
				}
				$kab=$this->input->post('kab');
				$idkab=$kab;
				if ($kab>0) {
					$kab=$this->anak_model->getkab($kab);
					$kab=$kab[0]['nama'];
				}
				$kec=$this->input->post('kec');
				$idkec=$kec;
				if ($kec>0) {
					$kec=$this->anak_model->getkec($kec);
					$kec=$kec[0]['nama'];
				}
				$desa=$this->input->post('desa');
				$iddesa=$desa;
				if ($desa>0) {
				$desa=$this->anak_model->getdes($desa);
				$desa=$desa[0]['nama'];
				}
				$NIK=$this->input->post('NIK');
				$nama=$this->input->post('nama');
				$akta=$this->input->post('akta');
				$KK=$this->input->post('KK');
				$tanggallahir=$this->input->post('tanggallahir');
				$tanggalentri=$this->input->post('tanggalentri');

				$data['hasil']=$this->anak_model->search($idprov,$idkab,$idkec,$iddesa,$prov,$kab,$kec,$desa,$NIK,$nama,$akta,$KK,$tanggallahir,$tanggalentri);
				}
		$this->load->view('header');
		$this->load->view('daftarkia',$data);
		$this->load->view('footer');
	}

	public function statusdokumen()
	{
		$data['form']=$this->input->post();
		if ($data['form'])
		{
			$mulai=$this->input->post('mulai');
			$mulai = date('Y-m-d', strtotime($mulai));
			$selesai=$this->input->post('selesai');
			$selesai = date('Y-m-d', strtotime($selesai));
		}
		else
		{
			$mulai=date("Y-m-d");
			$selesai=date("Y-m-d");
		}

		$data['hasil']=$this->anak_model->dokumenadmin($mulai,$selesai);
		$i=0;
		foreach ($data['hasil'] as $item) {
		$nik=$item['nomor_nik'];
		$status=$this->anak_model->getuser($nik);
		$status=$status[0]['registrasi'];
		$data['hasil'][$i]['statusdokumen']=$status;
		$i=$i+1;
		}
		$this->load->view('header');
		$this->load->view('statusdokumenadmin',$data);
		$this->load->view('footer');
	}

}
