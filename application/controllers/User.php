<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 public function __construct()
			{
			 parent::__construct();
			 $this->load->model('anak_model');
			 if (( $this->session->userdata('userlevel') <>1 ))
			 {
				// redirect('login');
			 }
			 else { if ($this->session->userdata('userlevel')==2) { redirect('admin'); }
			  }

			}
	 public function index()
 	{
		$data['kosong']=0;
 		$get_prov = $this->db->select('*')->from('wilayah_provinsi')->get();

 		$data['provinsi'] = $get_prov->result();
 		$data['form']=$this->input->post();
 		$data['search']=0;
 		if ($data['form'])
 		{

				$NIK=$this->input->post('NIK');
				$nama=$this->input->post('nama');
				$akta=$this->input->post('akta');
				$KK=$this->input->post('KK');
				$tanggallahir=$this->input->post('tanggallahir');
				$tanggalentri=$this->input->post('tanggalentri');

				if (!empty($NIK) || !empty($nama) || !empty($tanggallahir) || !empty($KK) || !empty($tanggalentri) || $akta <> 3 )
				{
					$data['search']=1;
				$data['hasil']=$this->anak_model->usersearch($NIK,$nama,$akta,$KK,$tanggallahir,$tanggalentri);
				//echo var_dump($data['hasil']);
				}
				else { $data['kosong']=1;  }
 				}
 		$this->load->view('header');
 		$this->load->view('index',$data);
 		$this->load->view('footer');
 	}

	public function photo($NIK)
	{
		$data['form']=$this->input->post();
		if ($data['form'])
		{
			if ($_FILES['photo']['error'] <> 4)
			{
				$config['upload_path']   = './upload/';
					     							$config['allowed_types'] = '*';
							 							$config['encrypt_name'] = true;
					     							//$this->load->library('upload', $config);
														$this->load->library('upload',$config);
													if ( !$this->upload->do_upload('photo'))
														{
															//echo var_dump($this->input->post());
															echo "gagal";
															$this->upload->display_errors();
															echo "<br>";
															echo var_dump($this->upload->data());
														}
														else
														{
															$upload_data=$this->upload->data();
															$file=$upload_data['file_name'];
															$data['forminput'] = array (
																'photo' => $file
															);
															echo "sukses";
															$result=$this->anak_model->updatephoto($file,$NIK);

														}
			}
		}

		$data['user']=$this->anak_model->getuser($NIK);
		$this->load->view('header');
		$this->load->view('photo',$data);
		$this->load->view('footer');
	}

	public function biodataanak()
	{
		$this->load->view('header');
		$this->load->view('biodataanak');
		$this->load->view('footer');
	}

	public function statusdokumen()
	{
		$user=$this->session->userdata('nik');
		$data['hasil']=$this->anak_model->dokumen($user);
		$i=0;

		foreach ($data['hasil'] as $item) {
		$nik=$item['nomor_nik'];
		$status=$this->anak_model->getuser($nik);
		$status=$status[0]['registrasi'];
		$data['hasil'][$i]['statusdokumen']=$status;
		$i=$i+1;
		}
		$this->load->view('header');
		$this->load->view('statusdokumen',$data);
		$this->load->view('footer');
	}

	public function registrasi($NIK)
	{


			$data['regis']=$this->anak_model->getuser($NIK);
			$data['regis']=$data['regis'][0];
			//echo var_dump($data['regis']);
			$no_nik=$data['regis']['nomor_nik'];
			$no_kk=$data['regis']['nomor_kk'];
			$tanggal_daftar=date("Y-m-d");
			$date = DateTime::createFromFormat('Y-m-d', $tanggal_daftar);
			$date->modify('+3 day');
			$tanggal_ambil = $date->format('Y-m-d H:i:s');
			$max=$this->anak_model->maxantri();
			$nomor_antrian=$max[0]["nomor_antrian"]+1;
			$user=$this->session->userdata('nik');
			$namaanak=$data['regis']['nama_anak'];
			$foto=$data['regis']['photo'];
			$result=$this->anak_model->userregis($NIK);
			$cek=$this->anak_model->cekdokumen($NIK);
			if (!$cek) {
			$data['forminput'] = array(
				'nomor_nik' => $NIK,
				'nomor_kartu_keluarga' => $no_kk,
				'tanggal_daftar' => $tanggal_daftar,
				'tanggal_ambil' => $tanggal_ambil,
				'nomor_antrian' => $nomor_antrian,
				'nama_anak' => $namaanak,
				'foto' => $foto,
				'user' => $user

				);
			$result=$this->anak_model->tambahdokumen($data['forminput']);
			}

			if ($cek) {
			$data['forminput'] = array(
				'nomor_kartu_keluarga' => $no_kk,
				'tanggal_daftar' => $tanggal_daftar,
				'tanggal_ambil' => $tanggal_ambil,
				'nomor_antrian' => $nomor_antrian,
				'nama_anak' => $namaanak,
				'foto' => $foto,
				'user' => $user

				);
			$result=$this->anak_model->updatedokumen($data['forminput'],$NIK);
			}
		redirect ('user');
	}

}
