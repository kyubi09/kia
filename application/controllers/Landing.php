<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 public function __construct()
		 	{
			 parent::__construct();
			 $this->load->model('login_model');


			}
	public function index()
	{
		$data['daftarsukses']=0;
		$data['nikdouble']=0;
		$data['emaildouble']=0;
		$data['salah']=0;
		$data['form']=$this->input->post();
		//echo var_dump($data);//untuk mengecek ada tidaknya post data ke url ini
		if ($data['form']) //jika ada post, masuk ke sini
		{ if ($data['form']['id']==1) {
			$username=$data["form"]["username"];
			$password=$data["form"]["password"];
			//echo $username.'<br>'.$password;
			$is_valid = $this->login_model->validate($username, $password);

			if ($is_valid)
			{ $user=$this->login_model->getuser($username);
				//echo var_dump($user);
				$level=$user[0]['level'];
				$nik=$user[0]['nik'];
				$nama=$user[0]['nama'];
				echo "<br>".$level;
				$data = array(
				'is_logged_in' => true,
				'userlevel' => $level,
				'nama' => $nama,
				'nik' => $nik
			     );
			$this->session->set_userdata($data);

			redirect('utama');
			}
			else
			{
				$data['salah']=1;
			}
		} //end if login

		if ($data['form']['id']==2)
			{

					$NIK=$data["form"]["NIK"];
					$nama=$data["form"]["nama"];
					$email=$data["form"]["email"];
					$password=$data["form"]["password"];
					$tempatlahir=$data["form"]["tempatlahir"];
					$tgllahir=$data["form"]["tgllahir"];
					$tgllahir= date("Y-m-d", strtotime($tgllahir));
					$telpon=$data["form"]["telpon"];
					$jk=$data["form"]["jeniskelamin"];
					$data['emaildouble']=$this->login_model->cekemail($email);
					$data['nikdouble']=$this->login_model->ceknik($NIK);
					if ($data['emaildouble'] || $data['nikdouble'] )
					{
						goto z;
					}
					$data['forminput'] = array(
						'nik' => $NIK,
						'nama' => $nama,
						'username' => $email,
						'password' => $password,
						'tempat_lahir' => $tempatlahir,
						'telepon' => $telpon,
						'tanggal_lahir' => $tgllahir,
						'jenis_kelamin' => $jk


						);

					$result=$this->login_model->tambahuser($data['forminput']);
					$data['daftarsukses']=1;
			}

		}
		z:
		$this->load->view('landing',$data);
	}

	public function daftar()
	{
		$data['nikdouble']=0;
		$data['emaildouble']=0;

		$data['form']=$this->input->post();
		//echo var_dump($data);//untuk mengecek ada tidaknya post data ke url ini
		if ($data['form']) //jika ada post, masuk ke sini
		{
		}
	}

}
