<?php

	class Login_model extends CI_Model
	{

		public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

		function tambahuser($data)
		{
			$this->db->insert('user', $data);
		}

		function validate($username, $password)
		{
		 	$this->db->where('username', $username);
		  $this->db->where('password', $password);
		  $query = $this->db->get('user');
		//  return $query->num_rows();

		  if($query->num_rows() == 1)
		 	{
		  	return true;
			}
			else
			{
				return false;
			}
		}
		function getuser($username)
		{
			$this->db->select('*');
			$this->db->where('username', $username);
		  $query = $this->db->get('user');
			return $query->result_array();
		}


		function cekemail($email)
		{
			$this->db->select('*');
			$this->db->where('username', $email);
			$query = $this->db->get('user');
			if($query->num_rows() == 1)
	  	{
	    	return true;
	  	}
			else
			{
				return false;
			}
		}
		function ceknik($nik)
		{
			$this->db->select('*');
			$this->db->where('nik', $nik);
			$query = $this->db->get('user');
			if($query->num_rows() == 1)
	  	{
	    	return true;
	  	}
			else
			{
				return false;
			}
		}

}
