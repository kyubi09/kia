<?php

	class Anak_model extends CI_Model
	{

		public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

	function getprov($id)
	{
			$this->db->select('nama');
			$this->db->where('id', $id);
		  $query = $this->db->get('wilayah_provinsi');
			return $query->result_array();
	}
	function getkab($id)
	{
			$this->db->select('nama');
			$this->db->where('id', $id);
		  $query = $this->db->get('wilayah_kabupaten');
			return $query->result_array();
	}
	function getkec($id)
	{
			$this->db->select('nama');
			$this->db->where('id', $id);
		  $query = $this->db->get('wilayah_kecamatan');
			return $query->result_array();
	}
	function getdes($id)
	{
			$this->db->select('nama');
			$this->db->where('id', $id);
		  $query = $this->db->get('wilayah_desa');
			return $query->result_array();
	}

	function search($idprov,$idkab,$idkec,$iddesa,$prov,$kab,$kec,$desa,$NIK,$nama,$akta,$KK,$tanggallahir,$tanggalentri)
	{
		$this->db->select('*');
		if ($idprov>0) {
		$this->db->where('provinsi', $prov);
		}
		if ($idkab>0) {
		$this->db->where('kota', $kab);
		}
		if ($idkec>0) {
		$this->db->where('kecamatan', $kec);
		}
		if ($iddesa>0) {
		$this->db->where('kelurahan', $desa);
		}
		if ($NIK>0) {
		$this->db->where('nomor_nik', $NIK);
		}
		$this->db->like('nama_anak', $nama);
		if ($KK>0) {
		$this->db->where('nomor_kk', $KK);
		}
		if ($tanggallahir>0) {
		$this->db->where('tanggal_lahir',$tanggallahir);
		}
		if ($tanggalentri>0) {
		$this->db->where('tanggal_ambil',$tanggalentri);
		}
		$query=$this->db->get('registrasi');
		return $query->result_array();
	}

	function usersearch($NIK,$nama,$akta,$KK,$tanggallahir,$tanggalentri)
	{
		$this->db->select('*');
		if ($NIK>0) {
		$this->db->where('nomor_nik', $NIK);
		}
		$this->db->like('nama_anak', $nama);
		if ($KK>0) {
		$this->db->where('nomor_kk', $KK);
		}
		if ($tanggallahir>0) {
		$this->db->where('tanggal_lahir',$tanggallahir);
		}
		if ($tanggalentri>0) {
		$this->db->where('tanggal_ambil',$tanggalentri);
		}
		$query=$this->db->get('registrasi');
		return $query->result_array();
	}

	function getuser($NIK)
	{
			$this->db->select('*');
			$this->db->where('nomor_nik', $NIK);
		  $query = $this->db->get('registrasi');
			return $query->result_array();
	}
	function dokumen($NIK)
	{
			$this->db->select('*');
			$this->db->where('user', $NIK);
			$query = $this->db->get('daftar_doku');
			return $query->result_array();
	}

	function dokumenadmin($mulai,$selesai)
	{
			$this->db->select('*');
			$this->db->where('tanggal_daftar >=',$mulai);
			$this->db->where('tanggal_daftar <=',$selesai);
			$query = $this->db->get('daftar_doku');

			return $query->result_array();
	}

	function updatephoto($file,$NIK)
	{
		$data = array(
						 'photo' => $file
					);
			$this->db->where('nomor_nik', $NIK);
			$this->db->update('registrasi', $data);
	}

	function userregis($NIK)
	{
		$data = array(
						 'registrasi' => 1
					);
			$this->db->where('nomor_nik', $NIK);
			$this->db->update('registrasi', $data);
	}
	function unregis($NIK)
	{
		$admin = $this->session->userdata('nama');
		$data = array(
						 'registrasi' => 0
					);
		$ganti = array(
						'admin_edit'=> $admin
					);
			$this->db->where('nomor_nik', $NIK);
			$this->db->update('registrasi', $data);

			$this->db->where('nomor_nik', $NIK);
			$this->db->update('daftar_doku', $ganti);
	}

	function tambahdokumen($data)
	{
			$this->db->insert('daftar_doku', $data);
	}
		function updateadmin($data,$NIK)
		{
		$this->db->where('nomor_nik', $NIK);
		$res = $this->db->update('registrasi', $data['forminput']);
		return  $res;
		//return $res;
		}

		function cekdokumen($nik)
		{
			$this->db->select('*');
			$this->db->where('nomor_nik', $nik);
			$query = $this->db->get('daftar_doku');
			if($query->num_rows() == 1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		function updatedokumen($data,$NIK)
		{
		$this->db->where('nomor_nik', $NIK);
		$res = $this->db->update('daftar_doku', $data);
		return  $res;
		//return $res;
		}

		function maxantri()
		{
		$today=date("Y-m-d");
		echo "harini".$today."sss";
		$this->db->where('tanggal_daftar',$today);
		$this->db->select_max('nomor_antrian');
		$query = $this->db->get('daftar_doku');
		return  $query->result_array();
		//return $res;
		}


}
